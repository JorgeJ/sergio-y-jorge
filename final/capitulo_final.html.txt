------------------------------------------------------------------------------------------ 
CAPÍTULO FINAL:
imagen: https://www.google.com/url?sa=i&url=https%3A%2F%2Fleagueoflegends.fandom.com%2Fes%2Fwiki%2FUdyr&psig=AOvVaw042lZLpklteZvuccm4vHKI&ust=1603968000012000&source=images&cd=vfe&ved=0CAIQjRxqFwoTCOCD-MmM1-wCFQAAAAAdAAAAABAD

info autor:Udyr es un superhombre; es un contenedor para el poder indomable de cuatro espíritus de animales primigenios. Al explotar las naturalezas animales de los espíritus, Udyr puede usar su fuerza única: el tigre tigre le proporciona velocidad y ferocidad; la tortuga tortuga, resistencia; el oso oso, poder; y el fénix fénix, la llama eterna. Con estos poderes combinados, Udyr puede aplacar a todo aquel que intente perjudicar el orden natural. 

lore lol: https://forums.comunidades.riotgames.com/t5/LATAM/ct-p/latam





El espíritu se acercó, dejando escapar un alarido feroz antes de abalanzarse sobre Udyr.
p: Esta vez no sería capaz de esquivarlo, de modo que cruzó los brazos ante sí y apretó los puños. Un halo de energía mágica lo rodeó y bloqueó la acometida letal del tigre espiritual.
p: 

El espíritu salió despedido hacia atrás y, tras recuperar la compostura, esbozó una sonrisa dejando entrever las fauces. Su cuerpo frígido crepitó con una energía feroz, y astilló los huesos de antiguas víctimas a su paso. El lugar era un cúmulo de muerte.

Udyr clavó ambas rodillas esta vez, con la cabeza gacha y el cuerpo palpitándole de dolor mientras el espíritu caminaba a su alrededor. 
p: Sentía como el suelo vibraba con cada paso.

"Así no voy a poder".

Apretó los dientes; la sangre le cayó de los labios al sentir un nuevo temblor.

La voces estallaron.

—Los débiles... ¡son presas!

Udyr alzó la vista y vio que el espíritu se precipitaba hacia él de nuevo con los ojos ávidos de sangre, tan abiertos que se veía reflejado a sí mismo, también ávido, pero de violencia.

"He de aceptar quién soy".
p: 

De pronto, del cuerpo de Udyr emanaron unas llamas doradas como un incendio incontrolable. La ira se apoderó de él y se puso a la altura del tigre espiritual.

—¡Por fin parece que la presa se ha decidido a pelear!

Udyr se precipitó hacia la bestia lanzando un rugido, saltó sobre una de sus patas y trepó la abrupta superficie aferrándose con las sangrientas manos a cualquier trozo de hielo con el que poder impulsarse. La criatura se sacudió, arañando al caminante espiritual con sus afilados salientes. 
p: Udyr soltó un alarido, deleitándose con su poderío. Su ira estaba por fin a la altura de la de su enemigo y ambos gozaban de la brutal beligerancia.

Entonces, dio un brinco formidable y logró aferrarse al lomo del espíritu, dejando caer sangre por los laterales de la bestia. Una energía espiritual brotó en su interior, un poder lo bastante fuerte como para apaciguar cualquier dolor. Las voces de los animales aullaron, descontroladas en su cabeza; los bramidos de los seres consumidos por el tigre y su propia furia desatada se fundieron en uno solo.

—¡No soy ninguna presa! —exclamó Udyr conforme lanzaba una ráfaga de golpes demoledores, generando una hilera de fisuras en el cuerpo del animal.

Sus manos rasgaron, rebanaron e hicieron pedazos al enemigo con desenfreno. 
p: Acto seguido, y aullando de pura rabia, se abalanzó sobre su cuello y hundió las fauces en el cuello del espíritu.

Esperaba que este se desplomara, que su cuerpo se desmembrara y que los bloques de hielo quedaran reducidos a polvo...

Sin embargo, todo se esfumó de un plumazo, incluidas las voces. ¿Habían llegado a aullar? ¿Se habían lamentado?

A lo lejos, oyó el chillido de un águila.

"Céntrate. Tranquilo".

Udyr cayó y se tambaleó sobre suelo firme. Estaba cerca del lago, todavía con la respiración acelerada, y vio como los últimos enemigos desaparecían. De repente, sonó un nuevo estruendo y se reincorporó a duras penas. El lago, como celebrando su victoria, empezó a derretirse. Poco a poco, los últimos pedazos de hielo se fundieron, y el nivel del agua subió hasta acoplarse a aquella tierra adusta y fría.

Udyr avanzó cojeando, recordando el ritual que había repetido incontables veces en Hirana. Ahuecó las manos y se echó agua en la cabeza, los hombros y la espalda a fin de enjuagarse las heridas y dejarlas limpias. Por último, dio un trago.

Observó su reflejo y vio a un hombre que le devolvía la mirada.
p: Herido, puesto a prueba y vivo.

"Soy quien soy".

Udyr solo oía el sonido del agua fluyendo, pero, aun así, no sonreía.

"Esta lucha no ha hecho más que empezar".
